package eu.wedgess.kneeopexercise;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import eu.wedgess.kneeopexercise.tasks.GetImageTask;
import eu.wedgess.kneeopexercise.utils.Utils;

/**
 * A simple {@link Fragment} subclass. To allow viewing of each exercise
 */
public class ExerciseFragment extends Fragment {

    public static final String TAG = ExerciseFragment.class.getSimpleName();
    private static final String ARG_EXERCISE = "mExercise";

    // the actual Exercise which we will be displaying
    private Exercise mExercise;


    public ExerciseFragment() {
        // Required empty public constructor
    }

    /**
     * Create a new instance of  this fragment using the provided parameters.
     *
     * @param exercise The exercise - {@link Exercise} which is {@link android.os.Parcelable}.
     * @return A new instance of fragment ExerciseFragment.
     */
    public static ExerciseFragment newInstance(Exercise exercise) {
        ExerciseFragment fragment = new ExerciseFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_EXERCISE, exercise);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        // get the parcelable from the fragments arguments
        if (getArguments() != null) {
            mExercise = getArguments().getParcelable(ARG_EXERCISE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_exercise, container, false);

        final ImageView exerciseImgIV = (ImageView) rootView.findViewById(R.id.iv_exercise);
        final ProgressBar imagePB = (ProgressBar) rootView.findViewById(R.id.pb_image_holder);
        final TextView exerciseInstructionsTV = (TextView) rootView.findViewById(R.id.tv_exercise_steps);

        StringBuilder sb = new StringBuilder();
        // for each of the lines of instructions
        for (String step : mExercise.getInstructions()) {
            sb.append("\n")
                    .append("\u2022 ") // add a bullet point
                    .append(step) // add the actual instruction step
                    .append("\n"); // new line to space it out
        }

        // make sure network is available before making the request
        if (Utils.isNetworkAvailable(getActivity())) {
            // get the exercises image from the URL
            new GetImageTask(new GetImageTask.ImageDownloadListener() {
                @Override
                public void onComplete(Bitmap bitmap) {
                    imagePB.setVisibility(View.GONE);
                    exerciseImgIV.setVisibility(View.VISIBLE);
                    if (bitmap != null) {
                        // set the bitmap into the imageview
                        exerciseImgIV.setImageBitmap(bitmap);
                    }
                }

                @Override
                public void onError(String error) {
                    // this will just display the error message
                    imagePB.setVisibility(View.GONE);
                    exerciseImgIV.setVisibility(View.VISIBLE);
                }
            }).execute(mExercise.getImageUrl());
        }

        exerciseInstructionsTV.setText(sb.toString());

        return rootView;
    }
}
