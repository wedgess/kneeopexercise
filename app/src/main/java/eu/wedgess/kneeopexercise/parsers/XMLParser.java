package eu.wedgess.kneeopexercise.parsers;

import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import eu.wedgess.kneeopexercise.utils.Constants;
import eu.wedgess.kneeopexercise.Exercise;

/**
 * * Implements {@link Parser} so can switch between parsers at run time
 * each parser has  their own implementation of {@link #parseExercise(String)}
 * <p>
 * Created by Gareth on 18/02/2017.
 */

public class XMLParser implements Parser {

    public static final String TAG = XMLParser.class.getSimpleName();

    @Override
    public ArrayList<Exercise> parseExercise(String data) {
        ArrayList<Exercise> exerciseList = new ArrayList<>();
        XMLParser parser = new XMLParser();
        Document doc = parser.getDomElement(data); // getting DOM element

        NodeList nl = doc.getElementsByTagName(Constants.KEY_EXERCISES);
        // looping through all item exercise <exercise>
        for (int i = 0; i < nl.getLength(); i++) {
            Element e = (Element) nl.item(i);
            // getting each of the child nodes
            int id = Integer.parseInt(parser.getValue(e, Constants.KEY_ID));
            String name = parser.getValue(e, Constants.KEY_NAME);
            String imgUrl = parser.getValue(e, Constants.KEY_IMG_URL);
            List<String> steps = new ArrayList<>();
            NodeList instructions = e.getElementsByTagName(Constants.KEY_INSTRUCTIONS);
            NodeList stepsNode = ((Element) instructions.item(0)).getElementsByTagName(Constants.KEY_STEPS);
            for (int j = 0; j < stepsNode.getLength(); j++) {
                steps.add(getElementValue(stepsNode.item(j)));
            }

            exerciseList.add(new Exercise(id, name, imgUrl, steps));
        }
        return exerciseList;
    }

    private Document getDomElement(String xml) {
        Document doc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();

            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));
            doc = db.parse(is);

        } catch (ParserConfigurationException e) {
            Log.e(TAG, "Error: " + e.getMessage());
            return null;
        } catch (SAXException e) {
            Log.e(TAG, "Error: " + e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e(TAG, "Error: " + e.getMessage());
            return null;
        }
        // return DOM
        return doc;
    }

    private String getValue(Element item, String str) {
        NodeList n = item.getElementsByTagName(str);
        return this.getElementValue(n.item(0));
    }

    private String getElementValue(Node elem) {
        Node child;
        if (elem != null) {
            if (elem.hasChildNodes()) {
                for (child = elem.getFirstChild(); child != null; child = child.getNextSibling()) {
                    if (child.getNodeType() == Node.TEXT_NODE) {
                        return child.getNodeValue();
                    }
                }
            }
        }
        return "";
    }
}
