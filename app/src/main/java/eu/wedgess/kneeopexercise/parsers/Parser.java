package eu.wedgess.kneeopexercise.parsers;

import java.util.ArrayList;

import eu.wedgess.kneeopexercise.Exercise;

/**
 * Created by Gareth on 18/02/2017.
 */

public interface Parser {

    ArrayList<Exercise> parseExercise(String data);
}
