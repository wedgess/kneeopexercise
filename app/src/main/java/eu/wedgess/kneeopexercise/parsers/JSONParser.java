package eu.wedgess.kneeopexercise.parsers;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import eu.wedgess.kneeopexercise.Exercise;
import eu.wedgess.kneeopexercise.utils.Constants;

/**
 * Implements {@link Parser} so can switch between parsers at run time
 * each parser has  their own implementation of {@link #parseExercise(String)}
 * <p>
 * Created by Gareth on 18/02/2017.
 */

public class JSONParser implements Parser {

    private static final String TAG = JSONParser.class.getSimpleName();

    @Override
    public ArrayList<Exercise> parseExercise(String data) {
        ArrayList<Exercise> exerciseList = new ArrayList<>();
        try {
            // root
            JSONObject rootObject = new JSONObject(data);
            // exercises array
            JSONArray exercises = rootObject.getJSONArray(Constants.KEY_EXERCISES);
            for (int j = 0; j < exercises.length(); j++) {
                JSONObject exerciseJSON = exercises.getJSONObject(j);
                // get data from the exercise JSON
                int id = exerciseJSON.getInt(Constants.KEY_ID);
                String name = exerciseJSON.getString(Constants.KEY_NAME);
                String imgUrl = exerciseJSON.getString(Constants.KEY_IMG_URL);
                // get instruction object
                JSONObject instructions = exerciseJSON.getJSONObject(Constants.KEY_INSTRUCTIONS);

                // for each of the steps add them to steps List
                List<String> steps = new ArrayList<>();
                JSONArray stepsArray = instructions.getJSONArray(Constants.KEY_STEPS);
                for (int i = 0; i < stepsArray.length(); i++) {
                    steps.add(stepsArray.getString(i));
                }
                exerciseList.add(new Exercise(id, name, imgUrl, steps));
            }
        } catch (JSONException e) {
            Log.e(TAG, "Error parsing JSON data: " + e.getMessage());
            e.printStackTrace();
        }
        return exerciseList;
    }
}
