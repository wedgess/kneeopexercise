package eu.wedgess.kneeopexercise.tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * AsyncTask to get image from a URL, use {@link ImageDownloadListener} for callback
 * of when the Bitmap has been received
 * Created by Gareth on 13/02/2017.
 */

public class GetImageTask extends AsyncTask<String, Integer, Bitmap> {

    // connection timeout
    private static final int TIMEOUT = 12000;

    // listener for when the image has been downloaded
    public interface ImageDownloadListener {
        void onComplete(Bitmap bitmap);
        void onError(String error);
    }

    private ImageDownloadListener mImageListener;

    public GetImageTask(ImageDownloadListener listener) {
        this.mImageListener = listener;
    }

    @Override
    protected Bitmap doInBackground(String... urls) {
        return getBitmapFromURL(urls[0]);
    }

    private Bitmap getBitmapFromURL(String strURL) {
        HttpURLConnection connection = null;
        try {
            URL url = new URL(strURL);
            // get image from url and decode input stream to Bitmap
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setConnectTimeout(TIMEOUT); // 12 second timeout
            connection.setReadTimeout(TIMEOUT + TIMEOUT/2); // 18 second timeout
            connection.connect();

            // get response code
            int status = connection.getResponseCode();
            switch (status) {
                // Status OK
                case 200:
                case 201:
                    // get InputStream
                    InputStream input = connection.getInputStream();
                    // decode input stream as bitmap
                    return BitmapFactory.decodeStream(input);
                default:
                    return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                // disconnect the connection
                connection.disconnect();
            }
        }
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        if (bitmap == null) {
            // error occurred send back error message
            mImageListener.onError("Could not retrieve image from URL");
        } else {
            // successfully retrieved image, send callback
            mImageListener.onComplete(bitmap);
        }
    }
}