package eu.wedgess.kneeopexercise.tasks;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * AsyncTask to read data from a URL and send it back to the calling class using the
 * {@link ReadWebDataListener}.
 * <p>
 * Created by Gareth on 19/02/2016.
 */

public class ReadWebDataTask extends AsyncTask<String, Integer, String> {

    private static final String TAG = ReadWebDataTask.class.getSimpleName();
    private static final int TIMEOUT = 20000;

    // interface for callbacks
    public interface ReadWebDataListener {
        void onComplete(String data);

        void onError(String error);
    }

    // the listener for when
    private ReadWebDataListener mReadWebDataListener;

    public ReadWebDataTask(ReadWebDataListener readWebDataListener) {
        this.mReadWebDataListener = readWebDataListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... urls) {
        return getWebData(urls[0]);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String data) {
        super.onPostExecute(data);
        mReadWebDataListener.onComplete(data);
    }

    /**
     * Reads data from a URL as a String
     *
     * @param urlStr - String URL of where to read data from
     * @return - the data as a String
     */
    private String getWebData(String urlStr) {
        HttpURLConnection connection = null;
        try {
            URL url = new URL(urlStr);
            // connect to url
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET"); // send GET request
            connection.setRequestProperty("Content-length", "0");
            connection.setUseCaches(true); // use cache
            connection.setAllowUserInteraction(false);
            connection.setConnectTimeout(TIMEOUT); // set timeout for connection
            connection.setReadTimeout(TIMEOUT); // set timeout for reading the content
            connection.connect(); // connect

            // get response code
            int status = connection.getResponseCode();

            switch (status) {
                // Status OK
                case 200:
                case 201:
                    // read each line from connection input stream adding to StringBuilder
                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    // read each line from buffered reader
                    while ((line = br.readLine()) != null) {
                        sb.append(line) // append line
                                .append("\n"); // append line break
                    }
                    // close BufferedReader
                    br.close();
                    // return the data as a string
                    return sb.toString();
                default:
                    // error something other than status code: OK
                    mReadWebDataListener.onError("Unexpected response status code: " + status);
                    break;
            }

        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
            mReadWebDataListener.onError("IOException: " + ex.getMessage());
        } finally {
            if (connection != null) {
                try {
                    // close connection
                    connection.disconnect();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return null;
    }
}
