package eu.wedgess.kneeopexercise.utils;

/**
 * Created by Gareth on 18/02/2017.
 */

public class Constants {

    private Constants() {
        // non instantiable
    }

    public static final String EXERCISES_URL_JSON = "https://garethwilliams.host/beacon/exercises.json";
    public static final String EXERCISES_URL_XML = "https://garethwilliams.host/beacon/exercises.xml";

    public static final String KEY_EXERCISES = "exercises";
    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_IMG_URL = "img_url";
    public static final String KEY_INSTRUCTIONS = "instructions";
    public static final String KEY_STEPS = "steps";
    public static final String PREF_KEY_JSON_PARSER = "prefKeyJson";
    public static final String PREF_KEY_XML_PARSER = "prefKeyXml";

    public static final String ARG_KEY_EXERCISE_LIST = "keyArgsExercisesList";
    public static final String BUNDLE_KEY_EXERCISE = "keyArgsExercise";

    public static final String STATE_KEY_SECOND = "keySecondCount";
    public static final String STATE_KEY_REP = "keyRepCount";
}

