package eu.wedgess.kneeopexercise.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;

import eu.wedgess.kneeopexercise.R;

import static java.security.AccessController.getContext;

/**
 * Provides item decoration (line divider) for RecyclerView Items
 * Idea taken from - https://goo.gl/axQpSv
 * <p>
 * Created by Gareth on 10/11/2016.
 */
public class RecyclerItemDivider extends RecyclerView.ItemDecoration {
    private Drawable mDivider;

    public RecyclerItemDivider(Drawable dividerDrawable) {
        this.mDivider = dividerDrawable;
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        // get number of items in the RecyclerView
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);
            // get child layout params
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mDivider.getIntrinsicHeight();
            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }
}
