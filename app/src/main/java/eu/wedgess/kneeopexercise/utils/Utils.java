package eu.wedgess.kneeopexercise.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Created by Gareth on 19/02/2017.
 */

public class Utils {

    public static final String TAG = Utils.class.getSimpleName();

    private Utils() {
        //non-instantiable
    }

    /**
     * Check if internet connection is available
     *
     * @param context
     * @return
     */
    public static boolean isNetworkAvailable(Context context) {
        try {
            // check for both mobile and wifi connection
            int[] networkTypes = {ConnectivityManager.TYPE_MOBILE, ConnectivityManager.TYPE_WIFI};
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork != null) { // connected to the internet
                for (int networkType : networkTypes) {
                    if (networkType == activeNetwork.getType()) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            // returning false now anyway
            Log.e(TAG, e.getMessage());
        }
        return false;
    }

    // handle shared preferences
    public static void setBooleanPref(String name, boolean value, Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(name, value).apply();
    }

    public static boolean getBooleanPref(String name, boolean def, Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(name, def);
    }
}
