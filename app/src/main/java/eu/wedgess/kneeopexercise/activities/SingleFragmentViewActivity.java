package eu.wedgess.kneeopexercise.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import eu.wedgess.kneeopexercise.Exercise;
import eu.wedgess.kneeopexercise.ExerciseFragment;
import eu.wedgess.kneeopexercise.R;
import eu.wedgess.kneeopexercise.utils.Constants;

public class SingleFragmentViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_fragment_view);

        // toolbar stuff & enable toolbars home button
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Exercise exercise = null;
        // if bundle is not null (shouldn't ever be NULL)
        if (getIntent().getExtras() != null) {
            // get exercise from bundle
            exercise = getIntent().getExtras().getParcelable(Constants.BUNDLE_KEY_EXERCISE);
            // set the activities toolbar title to that of the exercises name
            getSupportActionBar().setTitle(exercise.getName());
            // commit the fragment to the container
            getSupportFragmentManager()
                    .beginTransaction().replace(R.id.fl_fragment_container, ExerciseFragment.newInstance(exercise))
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // finish activity on home button click
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
