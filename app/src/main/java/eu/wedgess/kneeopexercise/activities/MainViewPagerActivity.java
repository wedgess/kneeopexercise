package eu.wedgess.kneeopexercise.activities;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import eu.wedgess.kneeopexercise.Exercise;
import eu.wedgess.kneeopexercise.ExerciseFragment;
import eu.wedgess.kneeopexercise.R;
import eu.wedgess.kneeopexercise.adapters.ViewPagerAdapter;
import eu.wedgess.kneeopexercise.utils.Constants;
import me.relex.circleindicator.CircleIndicator;

/**
 * Main ViewPager for displaying the different exercises and the countdown timer
 * displayed under each fragment.
 * <p>
 * Created by Gareth on 19/02/2017.
 */
public class MainViewPagerActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int DEFAULT_COUNTDOWN_INTERVAL = 1000;
    public static final int DEFAULT_COUNTDOWN_TIME = 5000;
    public static final int DEFAULT_REP_COUNT = 15;

    private ViewPager mViewPager;
    private TextView mTimeCountdownTV, mRepCountdownTV;
    private FloatingActionButton mStartTimerFAB;
    private static Set<MediaPlayer> sActivePlayers = new HashSet<>();

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Constants.STATE_KEY_SECOND, mTimeCountdownTV.getText().toString());
        outState.putString(Constants.STATE_KEY_REP, mRepCountdownTV.getText().toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_view_pager);

        if (getIntent().getExtras() != null) {
            // get the list of exercises from the bundle passed in the intent
            ArrayList<Exercise> exercises = getIntent().getExtras().getParcelableArrayList(Constants.ARG_KEY_EXERCISE_LIST);
            if (exercises != null) {
                LinkedHashMap<String, Fragment> pagerItems = new LinkedHashMap<>();
                // build up exe
                for (Exercise exercise : exercises) {
                    //Log.i(TAG, exercise.toString());
                    pagerItems.put(exercise.getName(), ExerciseFragment.newInstance(exercise));
                }

                bindActivity(pagerItems);
            }
        }

        if (savedInstanceState != null) {
            mTimeCountdownTV.setText(savedInstanceState.getString(Constants.STATE_KEY_SECOND));
            mRepCountdownTV.setText(savedInstanceState.getString(Constants.STATE_KEY_REP));
        }
    }

    private void bindActivity(LinkedHashMap<String, Fragment> frags) {
        // set up toolbar and set it as actionbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // get a reference to the ViewPager from the layout file
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), frags);
        // set-up ViewPager with adapter
        mViewPager.setAdapter(adapter);
        // don't refresh fragments when swiped, set offscreen limit as adapters count
        mViewPager.setOffscreenPageLimit(adapter.getCount());

        // initially set the page title
        if (mViewPager.getAdapter().getCount() > 0) {
            getSupportActionBar().setTitle(mViewPager.getAdapter().getPageTitle(0));
        }
        // listen to view pagers page chang so the toolbar title can be set
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // set position by getting title from adapter
                getSupportActionBar().setTitle(mViewPager.getAdapter().getPageTitle(position));
            }

            @Override
            public void onPageScrolled(int position, float offset, int offsetPixel) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mViewPager);

        mTimeCountdownTV = (TextView) findViewById(R.id.tv_secs_remaining);
        mRepCountdownTV = (TextView) findViewById(R.id.tv_reps_remaining);

        mTimeCountdownTV.setText(String.valueOf(DEFAULT_COUNTDOWN_TIME / DEFAULT_COUNTDOWN_INTERVAL));
        mRepCountdownTV.setText(String.valueOf(DEFAULT_REP_COUNT));
        mStartTimerFAB = (FloatingActionButton) findViewById(R.id.fab_start_timer);
        final Button decrementRepCounter = (Button) findViewById(R.id.btn_decrement_rep_count);
        final Button incrementRepCounter = (Button) findViewById(R.id.btn_increment_rep_count);
        mStartTimerFAB.setOnClickListener(this);
        decrementRepCounter.setOnClickListener(this);
        incrementRepCounter.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // on back arrow press
        if (item.getItemId() == android.R.id.home) {
            // finish activity
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    // media players onComplete listener
    private MediaPlayer.OnCompletionListener releaseOnFinishListener = new MediaPlayer.OnCompletionListener() {
        public void onCompletion(MediaPlayer mp) {
            // release media player object
            mp.release();
            // remove object from set
            sActivePlayers.remove(mp);
        }
    };

    @Override
    public void onClick(View view) {
        int repCountdown = Integer.parseInt(mRepCountdownTV.getText().toString());
        switch (view.getId()) {
            case R.id.btn_increment_rep_count:
                mRepCountdownTV.setText(String.valueOf(repCountdown + 1));
                break;
            case R.id.btn_decrement_rep_count:
                // don't allow decrementing from 1 to 0
                if (repCountdown > 1) {
                    mRepCountdownTV.setText(String.valueOf(repCountdown - 1));
                }
                break;
            case R.id.fab_start_timer:
                // lock orientation when timer is running to avoid it stopping on orientation change
                int currentOrientation = getResources().getConfiguration().orientation;
                if (currentOrientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                } else {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }
                // new media player item add to set this is the only way the sound doesn;t cut out
                // at the end. This was a issue that sometimes sound stopped before fully finished
                final MediaPlayer player = MediaPlayer.create(MainViewPagerActivity.this,
                        R.raw.countdown);
                sActivePlayers.add(player);
                // add complete listener which we release and remove clip
                player.setOnCompletionListener(releaseOnFinishListener);
                player.start();
                // disable fab when counter is running
                mStartTimerFAB.setEnabled(false);
                // set interval as lower that default interval so timer is more precise
                new CountDownTimer(DEFAULT_COUNTDOWN_TIME, DEFAULT_COUNTDOWN_INTERVAL - 500) {
                    public void onTick(long millisUntilFinished) {
                        // round to nearest milli-sec as timer is not precise enough
                        long fin = Math.round(millisUntilFinished + DEFAULT_COUNTDOWN_INTERVAL);
                        // set default timer text as 5 otherwise set text as rounded time
                        mTimeCountdownTV.setText(
                                fin > DEFAULT_COUNTDOWN_TIME
                                        ? String.valueOf(DEFAULT_COUNTDOWN_TIME / DEFAULT_COUNTDOWN_INTERVAL)
                                        : String.valueOf(fin / DEFAULT_COUNTDOWN_INTERVAL));
                    }

                    public void onFinish() {
                        // decrement rep counter when 1 rep is complete
                        int repCount = Integer.parseInt(mRepCountdownTV.getText().toString()) - 1;
                        // reset timer rep countdown if reps are complete
                        if (repCount == 0) {
                            mRepCountdownTV.setText(String.valueOf(DEFAULT_REP_COUNT));
                            // move to next exercise as all repititions have been completed
                            mViewPager.setCurrentItem(
                                    mViewPager.getCurrentItem() != mViewPager.getChildCount() ?
                                            mViewPager.getCurrentItem() + 1
                                            : mViewPager.getCurrentItem(), true);
                        } else {
                            // otherwise set the rep count
                            mRepCountdownTV.setText(String.valueOf(repCount));
                        }
                        // reset countdown timer time & re-enable FAB
                        mTimeCountdownTV.setText(String.valueOf(DEFAULT_COUNTDOWN_TIME / DEFAULT_COUNTDOWN_INTERVAL));
                        mStartTimerFAB.setEnabled(true);
                        // reset sensor to allow device to control orientation
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
                    }
                }.start();
                break;
        }
    }
}
