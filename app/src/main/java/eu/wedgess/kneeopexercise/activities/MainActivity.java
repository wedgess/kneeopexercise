package eu.wedgess.kneeopexercise.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import eu.wedgess.kneeopexercise.Exercise;
import eu.wedgess.kneeopexercise.R;
import eu.wedgess.kneeopexercise.adapters.ExerciseAdapter;
import eu.wedgess.kneeopexercise.parsers.JSONParser;
import eu.wedgess.kneeopexercise.parsers.Parser;
import eu.wedgess.kneeopexercise.parsers.XMLParser;
import eu.wedgess.kneeopexercise.tasks.ReadWebDataTask;
import eu.wedgess.kneeopexercise.utils.Constants;
import eu.wedgess.kneeopexercise.utils.RecyclerItemDivider;
import eu.wedgess.kneeopexercise.utils.Utils;

public class MainActivity extends AppCompatActivity implements ExerciseAdapter.RecyclerClickListener {

    public static final String TAG = MainActivity.class.getSimpleName();

    private RecyclerView mRecyclerView;
    private RelativeLayout mDownloadingContentLayout;
    private TextView mDownloadingMsgTV;
    private ProgressBar mDownloadPB;
    private MenuItem mJSONMenuItem, mXMLMenuItem;
    private ArrayList<Exercise> mParsedExerciseList;
    // parser which we can switch between XML and JSON
    private Parser mParser;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // save list on orientation change
        outState.putParcelableArrayList(TAG, mParsedExerciseList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindActivity();

        // if orientation change don't re download data from internet just restore from savedInstantState
        if (savedInstanceState != null) {
            // get list from savedInstanceState
            mParsedExerciseList = savedInstanceState.getParcelableArrayList(TAG);
            // if list is not null or empty set the recycler adapter
            if (mParsedExerciseList != null && !mParsedExerciseList.isEmpty()) {
                mRecyclerView.setAdapter(new ExerciseAdapter(mParsedExerciseList, MainActivity.this, getApplicationContext()));
                mDownloadingContentLayout.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
            }
            // otherwise display PB and text and download the content in AsyncTask
            else {
                mDownloadingContentLayout.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
                startDataDownloadTask();
            }
        } else {
            // start to download the data from internet
            startDataDownloadTask();
        }
    }

    // used to bind the activities views to the references
    private void bindActivity() {
        // set up toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // main recycler view to display list of exercises
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_exercises);
        mDownloadingContentLayout = (RelativeLayout) findViewById(R.id.rl_downloading);
        mDownloadingMsgTV = (TextView) findViewById(R.id.tv_downloading_content);
        mDownloadPB = (ProgressBar) findViewById(R.id.pb_downloading_content);
        // fab to start view pager
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startViewPager();
            }
        });
        // set dividers for RecyclerView
        mRecyclerView.addItemDecoration(new RecyclerItemDivider(
                ContextCompat.getDrawable(MainActivity.this, R.drawable.line_divider)));
    }

    /**
     * start to download the data from the internet by running the {@link ReadWebDataTask}
     */
    private void startDataDownloadTask() {
        // make sure there is an internet connection before trying to pull data from the web
        if (Utils.isNetworkAvailable(MainActivity.this)) {
            // check of we are parsing the XML or JSOn version (check if XMl is set in prefs)
            final boolean parseXML = Utils.getBooleanPref(Constants.PREF_KEY_XML_PARSER, true, MainActivity.this);
            Log.i(TAG, "Parsing data from " + (parseXML ? "XML" : "JSON") + " format");
            // start the task to pull data from internet
            new ReadWebDataTask(new ReadWebDataTask.ReadWebDataListener() {
                @Override
                public void onComplete(String data) {
                    if (data != null) {
                        // successful
                        try {
                            // set parser
                            mParser = parseXML ? new XMLParser() : new JSONParser();
                            mParsedExerciseList = mParser.parseExercise(data);
//                        for (Exercise e : mParsedExerciseList) {
//                            Log.i(TAG, e.toString());
//                        }
                            // set recyclerviews adapter with recieved data
                            mRecyclerView.setAdapter(new ExerciseAdapter(mParsedExerciseList, MainActivity.this, getApplicationContext()));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // hide PB and display content
                        mDownloadingContentLayout.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onError(final String error) {
                    Log.e(TAG, "Download data error: " + error);
                    // needs to run on ui thread as it gets called from doInBackground
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mDownloadPB.setVisibility(View.GONE);
                            mDownloadingMsgTV.setText(error);
                        }
                    });

                }
            }).execute(parseXML ? Constants.EXERCISES_URL_XML : Constants.EXERCISES_URL_JSON);
        } else {
            //Log.e(TAG, "No internet connection cannot get data");
            // no internet, so hide the list of items and no internet text
            if (mRecyclerView.getVisibility() == View.VISIBLE) {
                mRecyclerView.setVisibility(View.GONE);
                mDownloadingContentLayout.setVisibility(View.VISIBLE);
            }
            mDownloadingContentLayout.findViewById(R.id.pb_downloading_content).setVisibility(View.INVISIBLE);
            mDownloadingMsgTV.setText(R.string.no_internet);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // get JSON and XML menu items to set their values from preferences
        mJSONMenuItem = menu.findItem(R.id.json_parser);
        mXMLMenuItem = menu.findItem(R.id.xml_parser);
        if (mJSONMenuItem != null && mXMLMenuItem != null) {
            mJSONMenuItem.setChecked(Utils.getBooleanPref(Constants.PREF_KEY_JSON_PARSER, false, MainActivity.this));
            mXMLMenuItem.setChecked(Utils.getBooleanPref(Constants.PREF_KEY_XML_PARSER, true, MainActivity.this));
        } else {
            Log.i(TAG, "Items NULL");
            return false;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Allow to switch between XML and JSOn parser from menu options and store their values in preferences
        int id = item.getItemId();
        switch (id) {
            case R.id.xml_parser:
                boolean setXML = !item.isChecked();
                // set XML & JSON boolean is preferences
                Utils.setBooleanPref(Constants.PREF_KEY_XML_PARSER, setXML, MainActivity.this);
                Utils.setBooleanPref(Constants.PREF_KEY_JSON_PARSER, !setXML, MainActivity.this);
                // set this item checked and JSON item as the opposite
                item.setChecked(setXML);
                mJSONMenuItem.setChecked(!setXML);
                startDataDownloadTask();
                break;
            case R.id.json_parser:
                boolean setJSON = !item.isChecked();
                // set XML & JSON boolean is preferences
                Utils.setBooleanPref(Constants.PREF_KEY_JSON_PARSER, setJSON, MainActivity.this);
                Utils.setBooleanPref(Constants.PREF_KEY_XML_PARSER, !setJSON, MainActivity.this);
                // set this item checked and XML item as the opposite
                item.setChecked(setJSON);
                mXMLMenuItem.setChecked(!setJSON);
                startDataDownloadTask();
                break;
        }

//        Log.i(TAG, "XML set:" + Utils.getBooleanPref(Constants.PREF_KEY_XML_PARSER, false, MainActivity.this));
//        Log.i(TAG, "JSON set:" + Utils.getBooleanPref(Constants.PREF_KEY_JSON_PARSER, false, MainActivity.this));

        return super.onOptionsItemSelected(item);
    }

    // start the view pager activity if list of parsed exercises is not empty
    private void startViewPager() {
        if (mParsedExerciseList != null && !mParsedExerciseList.isEmpty()) {
            Bundle bundle = new Bundle();
            Intent intent = new Intent(MainActivity.this, MainViewPagerActivity.class);
            bundle.putParcelableArrayList(Constants.ARG_KEY_EXERCISE_LIST, (ArrayList) mParsedExerciseList);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    @Override
    public void onClick(int position) {
        // start single view activity, to view exercise on its own
        Intent intent = new Intent(MainActivity.this, SingleFragmentViewActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.BUNDLE_KEY_EXERCISE,
                ((ExerciseAdapter) mRecyclerView.getAdapter()).getItemAtPosition(position));
        intent.putExtras(bundle);
        startActivity(intent);
    }

    // network change receiver for starting download task when user comes back online
    // if the data hasn't been downloaded yet
    private BroadcastReceiver mNetworkBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // make sure network is available and then dismiss Snackbar
            if (Utils.isNetworkAvailable(context)) {
                // if the list is empty or null then re-download
                if (mParsedExerciseList == null || mParsedExerciseList.isEmpty()) {
                    // reconnected to internet so download the data
                    startDataDownloadTask();
                }
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        // register the network broadcast receiver
        registerReceiver(mNetworkBroadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        // unregister network change broadcast receiver
        unregisterReceiver(mNetworkBroadcastReceiver);
    }
}
