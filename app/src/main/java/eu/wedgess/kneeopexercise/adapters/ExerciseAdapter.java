package eu.wedgess.kneeopexercise.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import eu.wedgess.kneeopexercise.Exercise;
import eu.wedgess.kneeopexercise.R;
import eu.wedgess.kneeopexercise.tasks.GetImageTask;
import eu.wedgess.kneeopexercise.utils.Utils;

/**
 * Adapter class for the RecyclerViews items. Contains a click listener to view individual items.
 * <p>
 * Created by Gareth on 19/02/2017.
 */

public class ExerciseAdapter extends RecyclerView.Adapter<ExerciseAdapter.ExerciseViewHolder> {

    private ArrayList<Exercise> mItems;
    private Context mContext;
    private RecyclerClickListener mClickListener;

    // Callback for when item has been clicked in RecyclerView
    public interface RecyclerClickListener {
        void onClick(int position);
    }

    public ExerciseAdapter(ArrayList<Exercise> items, RecyclerClickListener clickListener, Context context) {
        this.mItems = items;
        this.mClickListener = clickListener;
        this.mContext = context;
    }

    @Override
    public ExerciseAdapter.ExerciseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflate the view of the exercise row
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_exercise, parent, false);
        return new ExerciseViewHolder(itemView, mClickListener);
    }

    @Override
    public void onBindViewHolder(ExerciseViewHolder holder, int position) {
        holder.setData(mItems.get(position), mContext);
    }

    @Override
    public int getItemCount() {
        return mItems == null ? 0 : mItems.size();
    }

    public Exercise getItemAtPosition(int position) {
        return mItems.get(position);
    }

    // ViewHolder containing the rows widgets (TextView & ImageView) - static to avoid memory leaks
    static class ExerciseViewHolder extends RecyclerView.ViewHolder {

        private TextView nameTV;
        private ImageView exerciseIV;
        private ProgressBar imgLoadingPB;

        ExerciseViewHolder(View itemView, final RecyclerClickListener clickListener) {
            super(itemView);

            // get reference to the view holders views
            nameTV = (TextView) itemView.findViewById(R.id.tv_exercise_name);
            exerciseIV = (ImageView) itemView.findViewById(R.id.iv_img_preview);
            imgLoadingPB = (ProgressBar) itemView.findViewById(R.id.pb_image);

            // if click listener is set then send back the position of the click
            if (clickListener != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        clickListener.onClick(getAdapterPosition());
                    }
                });
            }
        }

        // set the view holders data (accessed by the adapter)
        void setData(Exercise exercise, Context context) {
            if (Utils.isNetworkAvailable(context)) {
                // start the AsyncTask to get the image and allow the run in parallel
                new GetImageTask(new GetImageTask.ImageDownloadListener() {
                    @Override
                    public void onComplete(Bitmap bitmap) {
                        // hide progress bar when image has been downloaded
                        imgLoadingPB.setVisibility(View.GONE);
                        // show exercise image
                        exerciseIV.setVisibility(View.VISIBLE);
                        if (bitmap != null) {
                            // load bitmap when task is complete
                            exerciseIV.setImageBitmap(bitmap);
                        }
                    }

                    @Override
                    public void onError(String error) {
                        // this will just display the error image
                        imgLoadingPB.setVisibility(View.GONE);
                        exerciseIV.setVisibility(View.VISIBLE);
                    }
                }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, exercise.getImageUrl());
            }
            // set the exercise name
            nameTV.setText(exercise.getName());

        }
    }
}
