# Android Development & Web Services #
-----------------------------------------------------------

*	You are required to implement an Android application that accesses a Web Service. 
*	The Web Service will provide xml content appropriate to your App
*	Your app will use Androids HttpURLConnection to access the web service
*	Your application will:
*	Contain a class that reflects the web service data – eg. Flower Class in my example.
*	Use an XML parser to pull out the appropriate content for your app
*	Display the content in a List which will be formatted appropriately. 
*	Use Images in the List


### My Project ###

I decided to write a exercise application for patients recovering from a knee replacement.

### How my application looks ###
------------------------------------------------

![Alt text](http://i.imgur.com/KSAkGtx.png "Main Exercise List")

![Alt text](http://i.imgur.com/v2Dpl3e.png "Exercise Rep Counter & List")